#%%
import tensorflow as tf
import numpy as np
import pdb
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from tensorflow.contrib.model_pruning.python import pruning # pylint: disable=no-name-in-module
from tensorflow.contrib.model_pruning.python.layers import layers # pylint: disable=no-name-in-module


def next_batch(num, data, labels):
    '''
    Return a total of `num` random samples and labels.
    '''
    idx = np.arange(0 , len(data))
    np.random.shuffle(idx)
    idx = idx[:num]
    data_shuffle = [data[ i] for i in idx]
    labels_shuffle = [labels[ i] for i in idx]
    return np.asarray(data_shuffle), np.asarray(labels_shuffle)

#%% load data
epochs = 50
batch_size = 5000 # Entire training set
path = "./balanced_set/"

features_train = np.loadtxt(path+"features_train_14b.csv", delimiter=",")
features_test = np.loadtxt(path+"features_test_14b.csv", delimiter=",")
labels_train = np.loadtxt(path+"labels_train_14b.csv", delimiter=",")
labels_test = np.loadtxt(path+"labels_test_14b.csv", delimiter=",")

batches = int(len(features_train) / batch_size)
print(f'number of batches: {batches}')

#%% plot the data distribution
pltFlag = 1
if pltFlag == 1:
    plt.figure(1,figsize=(8,2))
    s = features_train.shape
    x = []
    y = []
    for row in range(0,s[0],1):
        x.append(sum(features_train[row,:]))
        y.append(labels_train[row,0])
    plt.subplot(1,2,1)
    plt.scatter(x,y)
    plt.title('Distribution of training data')
    plt.ylabel('Stability (0/1)')
    plt.xlabel('normalized gen output')
    plt.subplot(1,2,2)
    plt.hist(x,20)
    plt.show()
    plt.figure(1,figsize=(8,2)) # for some reason this line might has to be entered manually

#%% Define Placeholders
input_NN = tf.placeholder(tf.float32, [None, 4])
safetey_class = tf.placeholder(tf.float32, [None, 2])

# Define the model
# See: https://github.com/rockchip-linux/tensorflow/blob/master/tensorflow/contrib/model_pruning/python/layers/layers.py

layer1 = layers.masked_fully_connected(input_NN, 50)
layer2 = layers.masked_fully_connected(layer1, 50)
layer3 = layers.masked_fully_connected(layer2, 50)
logits = layers.masked_fully_connected(layer3, 2)

# Create global step variable (needed for pruning)
global_step = tf.train.get_or_create_global_step()
reset_global_step_op = tf.assign(global_step, 0)

# Loss function
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=safetey_class))

# Training op, the global step is critical here, make sure it matches the one used in pruning later
# running this operation increments the global_step
train_op = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(loss, global_step=global_step)

# Accuracy ops
correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(safetey_class, 1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

# Get, Print, and Edit Pruning Hyperparameters
pruning_hparams = pruning.get_pruning_hparams()
print("Pruning Hyperparameters:", pruning_hparams)

# Change hyperparameters to meet our needs
pruning_hparams.begin_pruning_step = 0
pruning_hparams.end_pruning_step = 250
pruning_hparams.pruning_frequency = 1
pruning_hparams.sparsity_function_end_step = 250
pruning_hparams.target_sparsity = .8

# Create a pruning object using the pruning specification, sparsity seems to have priority over the hparam
p = pruning.Pruning(pruning_hparams, global_step=global_step, sparsity=.8)
prune_op = p.conditional_mask_update_op()

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    # Train the model before pruning (optional)
    for epoch in range(epochs):
        for batch in range(batches):
            batch_xs, batch_ys = next_batch(batch_size,features_train,labels_train)
            sess.run(train_op, feed_dict={input_NN: batch_xs, safetey_class: batch_ys})

        # Calculate Test Accuracy every 10 epochs
        if epoch % 50 == 0:
            acc_print = sess.run(accuracy, feed_dict={input_NN: features_test, safetey_class: labels_test})
            print("Un-pruned model step %d test accuracy %g" % (epoch, acc_print))
            acc_print_train = sess.run(accuracy, feed_dict={input_NN: features_train, safetey_class: labels_train})
            print("Un-pruned model step %d train accuracy %g" % (epoch, acc_print_train))

    acc_print = sess.run(accuracy, feed_dict={input_NN: features_test, safetey_class: labels_test})
    print("Pre-Pruning accuracy on test set:", acc_print)
    acc_print_train = sess.run(accuracy, feed_dict={input_NN: features_train, safetey_class: labels_train})
    print("Pre-Pruning accuracy on training set:", acc_print_train)

    # compute the confusion matrix
    print("Sparsity of layers (should be 0)", sess.run(tf.contrib.model_pruning.get_weight_sparsity()))
    # hack to extract the weights and biases
    b0 = [v for v in tf.trainable_variables() if v.name == "fully_connected/biases:0"][0]
    b1 = [v for v in tf.trainable_variables() if v.name == "fully_connected_1/biases:0"][0]
    b2 = [v for v in tf.trainable_variables() if v.name == "fully_connected_2/biases:0"][0]
    b3 = [v for v in tf.trainable_variables() if v.name == "fully_connected_3/biases:0"][0]

    W0 = [v for v in tf.trainable_variables() if v.name == "fully_connected/weights:0"][0]
    W1 = [v for v in tf.trainable_variables() if v.name == "fully_connected_1/weights:0"][0]
    W2 = [v for v in tf.trainable_variables() if v.name == "fully_connected_2/weights:0"][0]
    W3 = [v for v in tf.trainable_variables() if v.name == "fully_connected_3/weights:0"][0]

    np.savetxt(path+"output/W0_np.csv", sess.run(W0), delimiter=",")
    np.savetxt(path+"output/W1_np.csv", sess.run(W1), delimiter=",")
    np.savetxt(path+"output/W2_np.csv", sess.run(W2), delimiter=",")
    np.savetxt(path+"output/W3_np.csv", sess.run(W3), delimiter=",")
    np.savetxt(path+"output/b0_np.csv", sess.run(b0), delimiter=",")
    np.savetxt(path+"output/b1_np.csv", sess.run(b1), delimiter=",")
    np.savetxt(path+"output/b2_np.csv", sess.run(b2), delimiter=",")
    np.savetxt(path+"output/b3_np.csv", sess.run(b3), delimiter=",")

    # Reset the global step counter and begin pruning
    sess.run(reset_global_step_op)
    for epoch in range(epochs):
        for batch in range(batches):
            batch_xs, batch_ys = next_batch(batch_size,features_train,labels_train)
            # Prune and retrain
            sess.run(prune_op)
            sess.run(train_op, feed_dict={input_NN: batch_xs, safetey_class: batch_ys})

        # Calculate Test Accuracy every 10 epochs
        if epoch % 50 == 0:
            acc_print = sess.run(accuracy, feed_dict={input_NN: features_test, safetey_class: labels_test})
            print("Pruned model step %d test accuracy %g" % (epoch, acc_print))
            print("Weight sparsities:", sess.run(tf.contrib.model_pruning.get_weight_sparsity()))
            acc_print_train = sess.run(accuracy, feed_dict={input_NN: features_train, safetey_class: labels_train})
            print("Pruned model step %d train accuracy %g" % (epoch, acc_print_train))

    # Print final accuracy
    acc_print = sess.run(accuracy, feed_dict={input_NN: features_test, safetey_class: labels_test})
    print("Final accuracy:", acc_print)
    acc_print_train = sess.run(accuracy, feed_dict={input_NN: features_train, safetey_class: labels_train})
    print("Final accuracy on training set:", acc_print_train)
    print("Final sparsity by layer (should be 0)", sess.run(tf.contrib.model_pruning.get_weight_sparsity()))

    tvars = tf.trainable_variables()
    tvars_vals = sess.run(tvars)

    mask = sess.run(tf.contrib.model_pruning.get_masks())

    W = [tvars_vals[0], tvars_vals[2], tvars_vals[4], tvars_vals[6]]
    W0 = W[0] * mask[0]
    W1 = W[1] * mask[1]
    W2 = W[2] * mask[2]
    W3 = W[3] * mask[3]

    b0 = tvars_vals[1]
    b1 = tvars_vals[3]
    b2 = tvars_vals[5]
    b3 = tvars_vals[7]
    
    #print(tf.contrib.model_pruning.get_weights())
    weights_p = tf.contrib.model_pruning.get_weights()
    weights_p_v = sess.run(weights_p)
    #print(weights_p_v)
    # command to show all attributes of a python object
    #print(dir(tf.contrib.model_pruning))
    #print(sess.run())
    # Desired variable is called "tower_2/filter:0".
    b0 = [v for v in tf.trainable_variables() if v.name == "fully_connected/biases:0"][0]
    b1 = [v for v in tf.trainable_variables() if v.name == "fully_connected_1/biases:0"][0]
    b2 = [v for v in tf.trainable_variables() if v.name == "fully_connected_2/biases:0"][0]
    b3 = [v for v in tf.trainable_variables() if v.name == "fully_connected_3/biases:0"][0]

    W0 = [v for v in tf.trainable_variables() if v.name == "fully_connected/weights:0"][0]
    W1 = [v for v in tf.trainable_variables() if v.name == "fully_connected_1/weights:0"][0]
    W2 = [v for v in tf.trainable_variables() if v.name == "fully_connected_2/weights:0"][0]
    W3 = [v for v in tf.trainable_variables() if v.name == "fully_connected_3/weights:0"][0]
    
    # hack to extract the weights and biases
    np.savetxt(path+"output/W0_p.csv", sess.run(W0), delimiter=",")
    np.savetxt(path+"output/W1_p.csv", sess.run(W1), delimiter=",")
    np.savetxt(path+"output/W2_p.csv", sess.run(W2), delimiter=",")
    np.savetxt(path+"output/W3_p.csv", sess.run(W3), delimiter=",")

    np.savetxt(path+"output/b0_p.csv", sess.run(b0), delimiter=",")
    np.savetxt(path+"output/b1_p.csv", sess.run(b1), delimiter=",")
    np.savetxt(path+"output/b2_p.csv", sess.run(b2), delimiter=",")
    np.savetxt(path+"output/b3_p.csv", sess.run(b3), delimiter=",")

# %%
