#%%
import pandas as pd
import scipy.io
import numpy as np
import tensorflow as tf


import NeuralNetwork_3layers_pruning as NN3

#%%
#path_db = '~/Users/jeanne.mermet-guyennet/Desktop/DTU/thesis/DB_case6/DB_final.mat'
path_db = 'case14db.mat' # classification/classDetails
#DB = scipy.io.loadmat(path_db)
DB = np.loadtxt("./balanced_set/NN_output.csv", delimiter=",")#DB['classification']

#%%
nn3 = NN3.Security([11, 100, 200, 100,  2], 0.005, 1, 0.001)
X_train, X_test, y_train, y_test, G_train, G_test = nn3.train_test_DB(DB, test_size=0)
crossval_3layer = nn3.train_and_prune(X_train, y_train, batch_size=500, nb_epochs=260, disp=True, sparsity=.7)

#%%
path = '3layer_final'
W1, b1, W2, b2, W3, b3,  W4, b4 = nn3.save_weights_bias(path)